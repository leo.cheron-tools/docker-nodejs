FROM alpine:latest

MAINTAINER Leo Cheron <leo@cheron.works>

# nodejs
RUN apk --no-cache add nodejs && \
    npm install pm2 --global
